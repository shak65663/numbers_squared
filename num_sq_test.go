package main

import (
	"sync"
	"testing"
)

func TestGetSqNum(t *testing.T) {
	var wg sync.WaitGroup
	cases := []struct {
		in   int
		want int
	}{
		{2, 4},
		{3, 9},
		{421, 177241},
		{-2, 4},
		{-3, 9},
		{-12, 144},
		{-421, 177241},
	}
	wg.Add(2)
	ch := make(chan int)
	go func() {
		defer wg.Done()
		for _, num := range cases {
			ch <- num.in
		}
	}()
	go func() {
		defer wg.Done()
		for _, num := range cases {
			result := GetSqNum(ch)
			if result != num.want {
				t.Errorf("Incorrect result. Expect %d, got %d", num.want, result)
			}
		}
	}()
	wg.Wait()
}
