package main

import (
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup
	ch := make(chan int)

	wg.Add(2)
	//This goroutine allows you to scan and check for errors entered values
	go func() {
		defer wg.Done()
		for {
			GetNum(ch)
		}
	}()

	//This goroutine reads a number from the channel and squares it
	go func() {
		defer wg.Done()
		for {
			numsq := GetSqNum(ch)
			fmt.Println("This is my number sqared:", numsq)
		}
	}()
	wg.Wait()
}

func GetNum(ch chan<- int) {
	var s string
	time.Sleep(1 * time.Millisecond)
	fmt.Printf("\rIt's my number: ")
	fmt.Scanln(&s)
	if s == "" {
		os.Exit(0)
	}
	num, err := strconv.Atoi(s)
	if err == nil {
		ch <- num
	} else {
		fmt.Println("Not a number!")
	}
}

func GetSqNum(ch <-chan int) int {
	num := <-ch
	numsq := num * num
	return numsq
}
